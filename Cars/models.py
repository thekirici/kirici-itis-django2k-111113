from django.db import models


class Category(models.Model):
    name = models.CharField(max_length=100)


class Order(models.Model):
    STATUS = (
        ('Pending', 'Pending'),
        ('Out for delivery', 'Out for delivery'),
        ('Delivered', 'Delivered'),
    )

    date_created = models.DateTimeField(auto_now_add=True, null=True)
    status = models.CharField(max_length=200, null=True, choices=STATUS)
    note = models.CharField(max_length=1000, null=True)

    def __str__(self):
        return self.product.name


class Car(models.Model):
    car_model = models.CharField(max_length=200)
    car_year = models.CharField(max_length=200)
    car_description = models.TextField()
    img = models.CharField(max_length=200)
    main = models.BooleanField(True)

    def __str__(self):
        return self.car_model + ' ' + self.car_year
