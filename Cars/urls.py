from django.urls import path
from . import views

urlpatterns = [
    path("", views.home, name='home'),
    path("home", views.home),
    path("cars", views.cars, name="cars"),
    path("form", views.form, name="form"),
    path("cars/<int:id>", views.description, name="details"),
    path("registerPage", views.register_page, name="registerPage"),
    path("loginPage", views.login_page, name="loginPage"),
    path('logout', views.logout_user, name="logout"),
]
