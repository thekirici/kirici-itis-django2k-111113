from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from .models import Car, Category
from .forms import CreateUserForm, AddCars


@login_required(login_url='loginPage')
def home(request):
    data = {
        "categories": Category.objects.all(),
        "cars": Car.objects.all().filter(main=True)
    }
    return render(request, "index.html", data)
    views.home


@login_required(login_url='loginPage')
def cars(request):
    data = {
        "categories": Category.objects.all(),
        "cars": Car.objects.all()
    }
    return render(request, "cars.html", data)


def description(request, id):
    data = {
        "id": id
    }
    return render(request, "details.html", data)


def register_page(request):
    if request.user.is_authenticated:
        return redirect('home')
    else:
        form = CreateUserForm()
        if request.method == 'POST':
            form = CreateUserForm(request.POST)
            if form.is_valid():
                form.save()
                user = form.cleaned_data.get('username')
                messages.success(request, 'Account was created for ' + user)

                return redirect('login')

        context = {'form': form}
        return render(request, 'registerPage.html', context)


def login_page(request):
    if request.user.is_authenticated:
        return redirect('home')
    else:
        if request.method == 'POST':
            username = request.POST.get('username')
            password = request.POST.get('password')

            user = authenticate(request, username=username, password=password)

            if user is not None:
                login(request, user)
                return redirect('home')
            else:
                messages.info(request, 'Username OR password is incorrect')

        context = {}
        return render(request, "loginPage.html", context)


def logout_user(request):
    logout(request)
    return redirect('loginPage')


@ login_required(login_url='loginPage')
def form(request):
    form1 = AddCars()

    if request.method == 'POST':
        form1 = AddCars(request.POST, request.FILES)
        if form1.is_valid():
            form1.save()

    context = {'form': form1}
    return render(request, "form.html", context)
